Accueil
=======

Bienvenue au Supinfo Low-Level Laboratory !
-------------------------------------------

Le *Supinfo Low-Level Laboratory* (abrégé *SL3*) est un laboratoire ayant pour
objectif de rassembler les élèves s'intéressant aux problématiques de
**bas-niveau**.

Ce laboratoire s'adresse aux personnes souhaitant décrouvrir et comprendre
comment fonctionne un **système d'exploitation**, un **langage de
programmation** et même une **machive virtuelle logicielle**.

De plus, c'est l'endroit idéal pour toute personne souhaitant approfondir ses
compétences de **programmation** : en se perfectionnant dans des langages comme
le **C**, le **C++**; en abordant des paradigmes nouveaux comme la
programmation **parallèle ou concurrente**.

Rejoindre la communauté
-----------------------

IRC
...

| serveur - irc.supnetwork.org
| salon - #sl3

Forum
.....

http://lab-sl3.org/forums/
